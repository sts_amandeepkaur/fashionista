from django.contrib import admin
from mysite.models import services,service,booking,contactus

admin.site.site_header = "Fashionista Admin"

class contactusAdmin(admin.ModelAdmin):
    list_display = ['id','first_name','last_name','email','subject','msz','added_on']
# Register your models here.
admin.site.register(services)
admin.site.register(service)
admin.site.register(booking)
admin.site.register(contactus,contactusAdmin)