from django.db import models
from django.contrib.auth.models import User

class services(models.Model):
    user = models.ForeignKey(User,on_delete= models.CASCADE)
    s_name = models.CharField(max_length=200)
    s_description=models.TextField()
    s_image = models.ImageField(upload_to='images/%Y/%m/%d',blank=True)
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.s_name

class service(models.Model):
    name = models.ForeignKey(services,on_delete=models.CASCADE)
    sub_name = models.CharField(max_length=200,null=True)
    image = models.ImageField(upload_to='images/%Y/%m/%d',blank=True)
    price=models.DecimalField(decimal_places=2,max_digits=10)
    offered_price=models.DecimalField(decimal_places=2,max_digits=10)
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sub_name

class booking(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250,blank=True)
    service = models.ForeignKey(services,on_delete=models.CASCADE)
    dateVisit = models.CharField(max_length=250,blank=True)
    timeVisit = models.CharField(max_length=250,blank=True)
    email = models.CharField(max_length=250,blank=True)
    contact = models.IntegerField(blank=True)
    notes = models.TextField(blank=True)
    booked_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.first_name

class contactus(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250,blank=True)
    subject = models.CharField(max_length=250,blank=True)
    email = models.EmailField(blank=True)
    msz = models.TextField(blank=True)
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.first_name



