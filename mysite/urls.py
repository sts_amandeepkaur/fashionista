from django.urls import path
from mysite import views
app_name="mysite"
urlpatterns = [
    path('contact/',views.contact,name="contact"),
    path('book/',views.book,name="book"),
    path('service/',views.allservice,name="service"),
    path('about/',views.about,name="about"),
    path('booking_detail/',views.booking_detail,name="booking_detail"),
]