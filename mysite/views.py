from django.shortcuts import render
from mysite.models import services,service,booking,contactus
from django.core.mail import EmailMessage
from django.http import HttpResponse, HttpResponseRedirect

all_services=services.objects.all().order_by('s_name')
lateset_service = services.objects.all().order_by('-id')[0]
def all_cats():
    #All Categories with sub categories
    main=[]
    for i in all_services:
        ab = []
        ab.append(i.s_name)
        ab += list(service.objects.filter(name=i).values())
        main.append(ab) 
    return main

def index(request):
    print(lateset_service.s_name)
    return render(request,'index.html',{'sr':all_services,'ls':lateset_service})

def contact(request):
    if request.method=="POST":
        fn = request.POST['fname']
        ln = request.POST['lname']
        em = request.POST['email']
        sub = request.POST['subject']
        msz = request.POST['message']

        data = contactus(first_name=fn,last_name=ln,email=em,subject=sub,msz=msz)
        data.save()
        return render(request,'contact.html',{'sr':all_services,'ls':lateset_service,'status':'Thanks for your feedback, we will contact you soon!!!'})

    return render(request,'contact.html',{'sr':all_services,'ls':lateset_service})

def book(request):
    if request.method == "POST":
        fn = request.POST['fname']
        ln = request.POST['lname']
        dt = request.POST['date']
        tm = request.POST['time']
        em = request.POST['email']
        con = request.POST['contact']
        srv = request.POST['treatment']
        note = request.POST['note']

        srv_obj = services.objects.get(id=srv)
        data = booking(first_name=fn,last_name=ln,service=srv_obj,dateVisit=dt,timeVisit=tm,email=em,contact=con,notes=note)
        data.save()
        # try:
        #     msz = 'Dear {} {} your service {} booked with service ID:{}\n See you on {} at {}'.format(fn,ln,srv_obj.s_name,data.id,dt,tm)
        #     mail = EmailMessage('Service Booking Summary', msz, to=[em,])
        #     mail.send()
        # except:
        #     rs = "Invalid email address or check your internet connection"
            # return render(request,'booking.html',{'sr':all_services,'ls':lateset_service,'result':rs})
        return HttpResponseRedirect('/mysite/booking_detail?id='+str(data.id))
    return render(request,'booking.html',{'sr':all_services,'ls':lateset_service})

def about(request):
    return render(request,'about.html',{'sr':all_services,'ls':lateset_service})

def allservice(request):
    all=all_cats()
    if 'id' in request.GET:
        id=request.GET['id']
        services = service.objects.filter(name__id=id)

        return render(request,'services.html',{'all':services,'srch':'1','sr':all_services,'ls':lateset_service})     
    return render(request,'services.html',{'all':all,'sr':all_services,'ls':lateset_service})

def booking_detail(request):
    if 'id' in request.GET:
        id = request.GET['id']
        booking_obj = booking.objects.get(id=id) 
    return render(request,'booking_details.html',{'book':booking_obj,'all':all,'sr':all_services,'ls':lateset_service})